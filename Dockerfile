# Use the official Ubuntu 18.04 as a base image
FROM ubuntu:18.04

# Set environment variables to avoid interactive prompts during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Install necessary packages
RUN apt-get update && apt-get install -y --no-install-recommends wget curl jq && rm -rf /var/lib/apt/lists/*

# Set the working directory
WORKDIR /app

# Copy the MalwareSourceCode folder and its subfolders into the Docker image
COPY MalwareSourceCode /app/MalwareSourceCode

# Download the EICAR test file and its variants without checking certificates
RUN wget --no-check-certificate -O eicar.com https://www.eicar.org/download/eicar.com \
    && wget --no-check-certificate -O eicar_com.zip https://www.eicar.org/download/eicar_com.zip \
    && echo "X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*" > eicar.txt

# Create a GTUBE test file for anti-spam testing
RUN echo "XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X" > gtube.txt


# Set the entry point for the container
CMD ["bash"]
