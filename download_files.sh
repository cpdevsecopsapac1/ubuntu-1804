#!/bin/bash

# Function to download files from a GitHub directory
download_directory() {
    local repo_url=$1
    local target_dir=$2
    local api_url="https://api.github.com/repos/vxunderground/MalwareSourceCode/contents/$repo_url"

    mkdir -p "$target_dir"
    files=$(curl -s "$api_url" | jq -r '.[] | select(.type == "file") | .download_url')

    for file_url in $files; do
        file_name=$(basename "$file_url")
        echo "Downloading $file_name to $target_dir"
        curl -L --insecure "$file_url" -o "$target_dir/$file_name"
    done
}

# Download specific directories
download_directory "Android" "MalwareSourceCode/Android"
download_directory "Perl" "MalwareSourceCode/Perl"

